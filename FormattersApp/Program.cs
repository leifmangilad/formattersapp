﻿using System;
using Formatters.BL;
using Formatters.Models;

namespace FormattersApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var formatter = new EntitiesFormatter(new FormatterProvider());

            var aEntity = new Entity
            {
                Id = 6,
                Data = "SecretData",
                DataType = "A"
            };
            var bEntity = new Entity
            {
                Id = 1,
                Data = "SomeData",
                DataType = "B"
            };

            var gilad_a_obj = formatter.Format("Gilad", aEntity).ToString();
            var gilad_b_obj = formatter.Format("Gilad", bEntity).ToString();
            var ohad_a_obj = formatter.Format("Ohad", aEntity).ToString();
            var ohad_b_obj = formatter.Format("Ohad", bEntity).ToString();

            Assert(gilad_a_obj, "{\r\n  \"id\": \"6\",\r\n  \"outputName\": \"A\",\r\n  \"data\": \"SecretData\"\r\n}");
            Assert(gilad_b_obj, "{\r\n  \"id\": \"1\",\r\n  \"outputName\": \"B\",\r\n  \"data\": \"ataDemoS\"\r\n}");
            Assert(ohad_a_obj, "<entity>\r\n  <id>6</id>\r\n  <outputName>A</outputName>\r\n  <data>SecretData</data>\r\n</entity>");
            Assert(ohad_b_obj, "{\r\n  \"id\": \"1\",\r\n  \"outputName\": \"B\",\r\n  \"data\": \"ataDemoS\"\r\n}");
            Assert(gilad_b_obj, ohad_b_obj);
        }

        private static void Assert(string a, string b)
        {
            if (a != b)
                throw new NotImplementedException();
        }
    }
}