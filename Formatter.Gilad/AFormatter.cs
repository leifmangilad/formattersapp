﻿using System.Collections.Generic;
using System.Composition;
using Formatters.BL;
using Formatters.Models;
using Newtonsoft.Json.Linq;

namespace Formatter.Gilad
{
    [Export(typeof(IFormatter))]
    public class AFormatter : IFormatter
    {
        public FormatterKey Name { get; } = new FormatterKey("Gilad", "A");
        public object Format(Entity entity)
        {
            return JObject.FromObject(new Dictionary<string, string>
            {
                {"id", entity.Id.ToString()},
                {"outputName", entity.DataType},
                {"data", entity.Data}
            });
        }
    }
}
