﻿using System.Collections.Generic;
using System.Composition;
using Formatters.BL;
using Formatters.Models;

namespace Formatter.Gilad
{
    [Export(typeof(IFormatter))]
    [Export(typeof(IRootFormatter))]
    class GiladFormatter : ARootFormatter
    {
        public override FormatterKey Name { get; } = new FormatterKey("Gilad", "");

        public override Dictionary<string, FormatterKey> FormattersMapping { get; } =
            new Dictionary<string, FormatterKey>
            {
                {"A", new FormatterKey("Gilad", "A")},
                {"B", new FormatterKey("Gilad", "B")}
            };
    }
}