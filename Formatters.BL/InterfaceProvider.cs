﻿using System.Collections.Generic;
using System.Composition;
using System.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;

namespace Formatters.BL
{
    public class InterfaceProvider<TImported>
    {
        public InterfaceProvider()
        {
            Compose();
        }

        [ImportMany] public IEnumerable<TImported> Importeds { get; private set; }


        private void Compose()
        {
            // Catalogs does not exists in Dotnet Core, so you need to manage your own.
            var assemblies = new List<Assembly>();
            var current = Directory.GetCurrentDirectory();
            var dllsPath = Path.Join(current, "Projects\\");
            var pluginAssemblies = Directory.GetFiles(dllsPath, "*.dll", SearchOption.TopDirectoryOnly)
                .Select(AssemblyLoadContext.Default.LoadFromAssemblyPath)
                // Ensure that the assembly contains an implementation for the given type.
                .Where(s => s.GetTypes().Any(p => typeof(TImported).IsAssignableFrom(p)));
            assemblies.AddRange(pluginAssemblies);
            var configuration = new ContainerConfiguration()
                .WithAssemblies(assemblies);
            using (var container = configuration.CreateContainer())
            {
                Importeds = container.GetExports<TImported>();
            }
        }


    }

}
