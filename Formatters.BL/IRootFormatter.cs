﻿namespace Formatters.BL
{
    public interface IRootFormatter : IFormatter
    {
        void Initialize(IFormatterProvider provider);
    }
}