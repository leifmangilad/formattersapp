﻿using System.Collections.Generic;
using System.Linq;
using Formatters.Models;

namespace Formatters.BL
{
    public class FormatterProvider : InterfaceProvider<IFormatter>, IFormatterProvider
    {
        public IDictionary<FormatterKey, IFormatter> FormattersMap { get; }

        public FormatterProvider()
        {
            FormattersMap = Importeds.ToDictionary(f => f.Name, f => f);
            
            foreach (var rootFormatter in FormattersMap.Values.OfType<IRootFormatter>())
            {
                rootFormatter.Initialize(this);
            }
        }

        public IFormatter GetFormatter(FormatterKey name)
        {
            return FormattersMap[name];
        }
    }
}