﻿using Formatters.Models;

namespace Formatters.BL
{
    public class EntitiesFormatter
    {
        private readonly IFormatterProvider _provider;

        public EntitiesFormatter(IFormatterProvider provider)
        {
            _provider = provider;
        }

        public object Format(string formatterName, Entity entity)
        {
            return _provider.GetFormatter(new FormatterKey(formatterName, ""))?.Format(entity);
        }
    }
}