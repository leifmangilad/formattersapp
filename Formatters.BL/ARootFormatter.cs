﻿using System.Collections.Generic;
using System.Linq;
using Formatters.Models;

namespace Formatters.BL
{
    public abstract class ARootFormatter : IRootFormatter
    {
        public abstract FormatterKey Name { get; }

        public abstract Dictionary<string, FormatterKey> FormattersMapping { get; }

        public Dictionary<string, IFormatter> Formatters;

        public object Format(Entity entity)
        {
            if (Formatters.TryGetValue(entity.DataType, out var formatter))
            {
                return formatter.Format(entity);
            }

            throw new FormatterNotFoundException();
        }

        public void Initialize(IFormatterProvider provider)
        {
            Formatters = FormattersMapping.ToDictionary(kw => kw.Key, kw => provider.GetFormatter(kw.Value));
        }
    }
}
