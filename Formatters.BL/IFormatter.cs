﻿using Formatters.Models;

namespace Formatters.BL
{
    public interface IFormatter
    {
        FormatterKey Name { get; }
        object Format(Entity entity);
    }
}
