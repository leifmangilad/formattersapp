﻿using Formatters.Models;

namespace Formatters.BL
{
    public interface IFormatterProvider
    {
        IFormatter GetFormatter(FormatterKey name);
    }
}
