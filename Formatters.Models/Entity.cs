﻿namespace Formatters.Models
{
    public class Entity
    {
        public int Id { get; set; }
        public string DataType { get; set; }
        public string Data { get; set; }
    }
}
