﻿namespace Formatters.Models
{
    public struct FormatterKey
    {
        public string ProjectName { get; }
        public string DataType { get; }

        public FormatterKey(string projectName, string dataType)
        {
            ProjectName = projectName;
            DataType = dataType;
        }
    }
}
