﻿using System.Composition;
using System.Xml.Linq;
using Formatters.BL;
using Formatters.Models;

namespace Formatters.Ohad
{
    [Export(typeof(IFormatter))]
    public class AFormatter : IFormatter
    {
        public FormatterKey Name { get; } = new FormatterKey("Ohad", "A");
        public object Format(Entity entity)
        {
            return new XElement("entity", 
                new XElement("id", entity.Id.ToString()),
                new XElement("outputName", entity.DataType),
                new XElement("data", entity.Data)
            );
        }
    }

}
