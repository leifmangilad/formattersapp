﻿using System.Collections.Generic;
using System.Composition;
using Formatters.BL;
using Formatters.Models;

namespace Formatters.Ohad
{
    [Export(typeof(IFormatter))]
    [Export(typeof(IRootFormatter))]
    class OhadFormatter : ARootFormatter
    {
        public override FormatterKey Name { get; } = new FormatterKey("Ohad", "");

        public override Dictionary<string, FormatterKey> FormattersMapping { get; } =
            new Dictionary<string, FormatterKey>
            {
                {"A", new FormatterKey("Ohad", "A")},
                {"B", new FormatterKey("Gilad", "")}
            };
    }
}